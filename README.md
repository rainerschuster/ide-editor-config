This repository contains a .editorconfig which can be included into different projects 
to format codes with a similar style.

To read more about .editorconfig visit [editorconfig.org](https://editorconfig.org/).

[Here](https://github.com/editorconfig/editorconfig/wiki/EditorConfig-Properties) A list of configurable properties with .editorconfig. 

### How to use

You can download/clone the repository and copy the .editorconfig to the root of your project.
A list of supporting IDEs can be found in editor config the website [main page](https://editorconfig.org/).


### Improvement/Contribution

If the configuration does not match your requirements or you want to add a language specific configuration, 
after you made your changes to your local copy, please also make a pull request or send a copy to me.  
